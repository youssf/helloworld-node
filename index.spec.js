require('jasmine');
var add = require('./index.js');
describe("Mon example de test NodeJs", function(){
    it("Should return 2 when adding 1+1", function(){
        let actual = add(5,5);
        expect(actual).toBe(10);
    })
});